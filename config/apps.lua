local apps = {
    terminal = "kitty",
    --launcher = "sh /home/parndt/.config/rofi/launch.sh", 
    --switcher = require("widgets.alt-tab"), 
    xrandr = "lxrandr",
    --screenshot = "scrot -e 'echo $f'", 
    volume = "pavucontrol",
    --appearance = "lxappearance", 
    browser = "firefox",
    fileexplorer = "thunar",
    --musicplayer = "pragha", 
    settings = "nvim /home/dhims/.comfig/awesome/"
}

user = {
    terminal = "kitty",
    browser = "firefox",
}

return apps

